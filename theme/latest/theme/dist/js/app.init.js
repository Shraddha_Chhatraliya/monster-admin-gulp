$(function() {
    "use strict";
    $("#main-wrapper").AdminSettings({
        Theme: false, // this can be true or false ( true means dark and false means light ),
        Layout: 'vertical',
        LogoBg: 'sidebar-skin-1', // You can change the Value to be sidebar-skin-1/sidebar-skin-2
        NavbarBg: 'header-skin-4', // You can change the Value to be header-skin-1/header-skin-2/header-skin-3/header-skin-4/header-skin-5/header-skin-6
        SidebarType: 'full', // You can change it full / mini-sidebar 
        SidebarColor: 'sidebar-skin-1', // You can change the Value to be sidebar-skin-1/sidebar-skin-2
        SidebarPosition: true, // it can be true / false ( true means Fixed and false means absolute )
        HeaderPosition: true, // it can be true / false ( true means Fixed and false means absolute )
        BoxedLayout: false, // it can be true / false ( true means Boxed and false means Fluid ) 
    });
});